drop table if exists uom;
create table uom (
  uom_code varchar(10),
  description varchar2(255)
  conversion_factor decimal (10,2)
  primary key (uom_code)
); 