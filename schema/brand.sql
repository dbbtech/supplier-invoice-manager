drop table if exists brand;
create table brand (
  brand_id int not null auto_increment,
  brand_name varchar(255),
  primary key (brand_id)
); 