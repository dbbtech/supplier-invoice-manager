drop table if exists customer;

create table customer (
  customer_id bigint not null auto_increment,
  first_name varchar(255) not null,
  last_name varchar(255) not null,
  middle_name varchar(255),
  address_line1 varchar(255),
  address_line2 varchar(255),
  city varchar(255),
  state_code varchar(45),
  zip int,
  email_address varchar(255),
  phone_number varchar(255) not null,
  password varchar(255),
  active varchar(1) not null default 'Y',
  type_code int not null default 1,
  reward_points bigint,
  date_created datetime,
  date_modified datetime,
  primary key (customer_id),
  unique key email_address_unique (email_address),
  unique key phone_number_unique (phone_number)
) comment='';

