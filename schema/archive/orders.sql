alter table order_line drop foreign key fk_order_upc_id;
alter table order_line drop foreign key fk_order_category_id;
alter table order_line drop foreign key fk_order_line_id;
alter table order_header drop foreign key fk_order_customer_id;

drop table if exists order_line;
drop table if exists order_header;

create table order_header (
  order_id bigint not null auto_increment,
  order_date datetime not null,
  register_id int not null,
  clerk_id int not null,
  status varchar(45) not null,
  sales_amount decimal(18,2) not null,
  tax_amount decimal(18,2),
  tax_exempt_id varchar(255),
  coupon_code varchar(255),
  discount_amount decimal(18,2),
  customer_id bigint,
  bill_address_line1 varchar(255),
  bill_address_line2 varchar(255),
  bill_city varchar(255),
  bill_state_code varchar(45),
  bill_zip int,
  ship_address_line1 varchar(255),
  ship_address_line2 varchar(255),
  ship_city varchar(255),
  ship_state_code varchar(45),
  ship_zip int,
  tender_type varchar(45),
  source varchar(45),
  primary key(order_id)
);

create table order_line (
  line_id bigint not null auto_increment,
  order_id bigint not null,
  product_id bigint,
  category_id int not null,
  upc_id bigint,
  unit_cost decimal(18,2) not null,
  price decimal(18,2) not null,
  qty_sold decimal(18,2) not null,
  coupon_code varchar(255),
  discount_amount decimal(18,2),
  reward_points bigint,
  status varchar(45) not null,
  primary key(line_id)
);

alter table order_header add index fk_order_customer_id (customer_id);
alter table order_header add constraint fk_order_customer_id foreign key (customer_id) references customer (customer_id);

alter table order_line add index fk_order_line_id (order_id);
alter table order_line add constraint fk_order_line_id foreign key (order_id) references order_header (order_id);

alter table order_line add index fk_order_category_id (category_id);
alter table order_line add constraint fk_order_category_id foreign key (category_id) references product_category (category_id);

alter table order_line add index fk_order_upc_id (upc_id);
alter table order_line add constraint fk_order_upc_id foreign key (upc_id) references product_upc (upc_id);
