alter table supplier_item drop foreign key fk_supplierid;
drop table if exists supplier_item;
drop table if exists supplier;

create table supplier (
  supplier_id int not null auto_increment ,
  name varchar(255) not null,
  address_line1 varchar(255) not null,
  address_line2 varchar(255),
  city varchar(255) not null,
  state varchar(255) not null,
  zip int not null,
  phone_number varchar(255) not null,
  fax_number varchar(255) null ,
  email_address varchar(255) null ,
  contact_first_name varchar(255),
  contact_last_name varchar(255),
  date_created datetime,
  date_modified datetime,
  primary key (supplier_id)
);

create table supplier_item (
  si_id bigint not null auto_increment,
  supplier_id int not null,
  supplier_item_id varchar(255) not null,
  description varchar(500) not null,
  pack_qty int not null,
  pack_cost decimal(18,2) not null,
  brand varchar(255),
  unit_weight decimal(10,2),
  uom varchar(255),
  date_created datetime,
  date_modified datetime,
  primary key (si_id)
); 

alter table supplier_item add index fk_supplierid (supplier_id); 
alter table supplier_item add constraint fk_supplierid foreign key (supplier_id) references supplier (supplier_id);
alter table supplier_item add unique index udx_supplier_supplier_item (supplier_id,supplier_item_id);
