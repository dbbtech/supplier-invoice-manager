alter table product_upc drop foreign key fk_productid;
alter table product_image drop foreign key fk_product_image_id;
alter table product drop foreign key fk_taxcode;
alter table product drop foreign key fk_categoryid;

drop table if exists product_upc;
drop table if exists product_image;
drop table if exists product;
drop table if exists product_category;

create table product_category (
  category_id int not null auto_increment,
  description varchar(255) not null,
  date_created datetime,
  date_modified datetime,
  primary key (category_id)
);

create table product (
  product_id bigint not null auto_increment,
  description varchar(2000) not null,
  receipt_description varchar(45) not null,
  label_description varchar(500),
  category_id int not null,
  unit_cost decimal(18,2) not null,
  price decimal(18,2) not null,
  qty_on_hand decimal(18,2),
  reorder_point decimal(18,2),
  reorder_qty decimal(18,2),
  tax_code varchar(5),
  sold_by_weight varchar(1) default 'N',
  active varchar(1) not null default 'Y',
  date_created datetime,
  date_modified datetime,
  primary key (product_id)
);

create table product_image (
  image_id bigint not null auto_increment,
  product_id bigint not null,
  image_type varchar(45) not null,
  image_url varchar(2000) not null,
  date_created datetime,
  date_modified datetime,
  primary key (image_id)
);


create table product_upc (
  upc_id bigint not null,
  product_id bigint not null,
  date_created datetime,
  date_modified datetime,
  primary key (upc_id)
);

alter table product add index fk_categoryid (category_id);
alter table product add constraint fk_categoryid foreign key (category_id) references product_category (category_id);

alter table product add index fk_taxcode (tax_code);
alter table product add constraint fk_taxcode foreign key (tax_code) references tax (tax_code);

alter table product_image add index fk_product_image_id (product_id);
alter table product_image add constraint fk_product_image_id foreign key (product_id) references product (product_id);

alter table product_upc add index fk_productid (product_id);
alter table product_upc add constraint fk_productid foreign key (product_id) references product (product_id);
