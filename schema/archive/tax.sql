drop table if exists tax;

create table tax (
  tax_code varchar(5) not null,
  description varchar(255) not null,
  date_created datetime,
  date_modified datetime,
  primary key (tax_code)
);
